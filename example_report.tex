\input{report_template.tex}
\author{Performed by: Your Name Here}
\rohead{Your Name Here}
\lohead{EGR 103 Circuit Analysis Laboratory}
%\titlehead{Grand Valley State University}
\title{Circuit Analysis Laboratory}
%\subtitle{Circuit Analysis}
\subject{EGR 103: Engineering Measurement and Analysis\\Winter 2020\\Instructor: Prof. Name Here}
\author{Your Name Here} 

\begin{document}
 
\maketitle

\section{Executive Summary}
This laboratory attempts to verify Kirchhoff's Voltage Law (KVL) and Kirchhoff's Current Law (KCL) through the use of resistive circuit measurements.  A circuit with a voltage source and six resistors was constructed to determine if KVL and KCL correctly predicted the voltage at each node and the current through each branch.  The results were accurate to within 10\% of the nominal resistor values providing some confidence in the use of KVL and KCL.
% * <zuidemsc@gvsu.edu> 00:02:58 20 Feb 2020 UTC-0500:
% Apparently you can leave a comment.  Does this show up in the source file?

\section{Introduction}
The purpose of this experiment was to determine if KVL and KCL can correctly predict the voltage at each node and the current through each branch in a circuit composed of a voltage source and six resistors.  Current and voltage measurements were taken using a digital multimeter (DMM).  The results were compared to the values determined from the KVL and KCL equations.

\section{Theory}
KVL states that the voltages around a loop in a circuit will add up to 0V.  The equation that expresses this law is show in Equation \ref{KVL}.
\begin{equation}\label{KVL}
    V_1+V_2+V_3+...+V_n=0
\end{equation}\\

KCL states that the currents entering a node in a circuit will add up to 0A.  The equation that expresses this law is show in Equation \ref{KCL}.
\begin{equation}\label{KCL}
    I_1+I_2+I_3+...+I_n=0
\end{equation}

\section{Apparatus}
\begin{enumerate}
    \item 1.5k, 1.3k, 1k, 6.8k, 1.6k, 5.6k Ohm, 5\% tolerance, $\frac{1}{4}$Watt resistors
    \item Circuit Design Trainer:  Grand Valley State Colleges 18758, \#6, ET-1000 Zenith HealthKit
    \item DDM: Tektronix TX3 True RMS Multimeter, SN \#xxxxxxxx
\end{enumerate}

\section{Experimental Setup}
The circuit constructed is shown in Figure \ref{fig:circuit}.  
\begin{figure}[h!]
    \centering 
    \includegraphics[width=1\textwidth]{images/circuit.png}
    \caption{Diagram of Circuit Constructed and Measured}
    \label{fig:circuit}
\end{figure}

The circuit in Figure \ref{fig:circuit} was constructed using resistor values as shown in Table \ref{tab:table1}.

\begin{center}
    \begin{table}[h!]%
        \caption{Circuit Analysis \label{tab:table1}}
    \begin{tabularx}{1\linewidth}{|C|C|C|C|C|}
    \hline
    \rowcolor{lightgray}
    Location & Nominal Resistor Value ($k$) & Tested Resistor Value (k) \\
    \hline
    $R_1$&1.5&1.53\\
    \hline
    $R_2$&1.3&1.30\\
    \hline
    $R_3$&1.0&1.00\\
    \hline
    $R_4$&6.8&6.76\\
    \hline
    $R_5$&1.6&1.59\\
    \hline
    $R_6$&5.6&5.63\\
    \hline
    \end{tabularx}
\end{table}
\end{center}

\textbf{Note for example:} Figure \ref{fig:circuit} can also be drawn inside of \LaTeX{} as shown in Figure \ref{fig:circuitLatex}.

\begin{figure}[h!]
    \centering 
    \caption{Diagram of Circuit Constructed and Measured - Created in \LaTeX{}}
    \label{fig:circuitLatex}
    \ctikzset{bipoles/length=1.4cm}
    \begin{circuitikz}[scale=1.0]\draw
    (0,4) to[V=5V, i=$i_S$] (0,0) 
    (0,4) to[R, a=$R_1$, l=1.5k\si{{\ohm}}] (2,4) %Format for just resistance and label, no V or I values
    (2,4) -- (3,4)
    (3,4) to[R=$R_2$ 1.3k\si{{\ohm}}, v=$V_2$, i=$i_2$] (5,4)
    (5,4) to[short, -*] (6,4)
    (6,4) to[R=$R_4$ 6800\si{{\ohm}}, v=$V_4$, i=$i_4$] (6,0)
    (6,4) to[short, *-] (7,4)
    (7,4) to[R=$R_3$ 1000\si{{\ohm}}, v=$V_3$, i=$i_3$] (9,4)
    (9,4) to[short, -*] (10,4)
    (10,4) to[R=$R_5$ 1600\si{{\ohm}}, v=$V_5$, i=$i_5$] (10,0)
    (10,4) -- (14,4) 
    (14,4) to[R=$R_6$ 5600\si{{\ohm}}, v=$V_6$, i=$i_6$] (14,0)
    (14,0) to[short, -*] (10,0) 
    (10,0) -- (0,0) 
    (0,0) node[ground] {}
    ;\end{circuitikz}\\
\end{figure}

\section{Experimental Procedure}
Resistance measurements were taken as part of the setup of the circuit.  Those values were taken using a DMM and the results are shown in Table \ref{tab:table1}.\\

KVL and KCL measurements were taken for the voltage across and the current through each resistor.  This was completed using a DMM.\\

Voltage measurements were completed by taken the DMM probes and placing one probe on each side of the resistor.  Once the voltage value had stabilized on the DMM, the value was recorded.\\

Current measurements were completed by removing one side of the resistor from the breadboard.  The DMM was placed in current mode with one probe connected to the now removed resistor lead.  The other DMM probe was connected to the breadboard location that the resistor was previously connected to.  This method forces the current to travel through the DMM so current can be measured.  Once the current value had stabilized on the DMM, the value was recorded.  The resistor was the returned to its original location on the breadboard.\\

\section{Sample Calculations}

Using KVL, Equations \ref{KVL_1}, \ref{KVL_2}, and \ref{KVL_3} equations were formed.
\begin{equation}\label{KVL_1}
    i_1*R_1+i_2*R_2+i_4*R_4=V_s
\end{equation}
\begin{equation}\label{KVL_2}
    i_3*R_3+i_5*R_5-i_4*R_4=0
\end{equation}
\begin{equation}\label{KVL_3}
    i_6*R_6-i_5*R_5=0
\end{equation}

Using KCL, Equations \ref{KCL_1} and \ref{KCL_2} equations were formed.
\begin{equation}\label{KCL_1}
    i_3+i_4=i_2
\end{equation}
\begin{equation}\label{KCL_2}
    i_3=i_5+i_6
\end{equation}

Using Equations \ref{KVL_1}-\ref{KCL_2} allowed the formation of a matrix of equations that can be solved.  That solution of that matrix is shown in Table \ref{tab:tableanalysis}.

\begin{center}
    \begin{table}[h!]%
        \caption{Circuit Analysis Results Nominal \label{tab:tableanalysis}}
    \begin{tabularx}{1\linewidth}{|C|C|C|C|C|}
    \hline
    \rowcolor{lightgray}
    Location & Nominal Resistor Value ($k$) & Voltage Across (V) & Current Through (A)\\
    \hline
    $R_1$&1.5&1.671&0.00111421\\
    \hline
    $R_2$&1.3&1.448&0.00111421\\
    \hline
    $R_3$&1.0&0.840&0.00083771\\
    \hline
    $R_4$&6.8&1.880&0.00027650\\
    \hline
    $R_5$&1.6&1.042&0.00065156\\
    \hline
    $R_6$&5.6&1.042&0.00018616\\
    \hline
    \end{tabularx}
\end{table}
\end{center}



\section{Results and Discussion}

The measured values for the circuit are listed in Table \ref{tab:tableresults} and the predicted values are in Table \ref{tab:tableanalysis}.  The results are within 10\% of the analysis values.

\begin{center}
    \begin{table}[h!]%
        \caption{Circuit Measurements \label{tab:tableresults}}
    \begin{tabularx}{1\linewidth}{|C|C|C|C|C|}
    \hline
    \rowcolor{lightgray}
    Location & Nominal Resistor Value ($k$) & Tested Resistor Value (k) & Voltage Across (V) & Current Across (A)\\
    \hline
    $R_1$&1.5&1.53&1.728&0.00111500\\
    \hline
    $R_2$&1.3&1.30&1.461&0.00111470\\
    \hline
    $R_3$&1.0&1.00&0.840&0.00083790\\
    \hline
    $R_4$&6.8&6.76&1.882&0.00027665\\
    \hline
    $R_5$&1.6&1.59&1.043&0.00064850\\
    \hline
    $R_6$&5.6&5.63&1.043&0.00018860\\
    \hline
    \end{tabularx}
\end{table}
\end{center}

Table \ref{tab:tableerror} contains the error calculations in percent for each resistor measured during this experiment.  \textbf{Note for example:} The data from this table is imported from a CSV file exported from Excel.

\begin{center}
    \begin{table}[h!]%
        \caption{Resistor Error Calculations \label{tab:tableerror}}
    \begin{tabularx}{1\linewidth}{|C|C|C|C|C|}
    \hline
    \rowcolor{lightgray}
    Location & Nominal Resistor Value ($k$) & Tested Resistor Value (k) & Error (\%)
    \csvreader[head to column names]{error_calculations.csv}{}% use head of csv as column names
    {\\\hline$\Location$&\Nominal&\Tested&\Error \%}% specify your columns here 
    \\
    \hline
    \end{tabularx}
\end{table}
\end{center}

\section{Conclusion}
The results indicate that using the KVL and KCL is an acceptable model for the circuit analysis.  The greatest error observed was for the Voltage across $R_1$.  That error was found to be 3.30\% from nominal even though the resistors used were 5.0\% in tolerance.

\newpage
\section{Appendix}
Should you have code to insert, Code \ref{code:projectcode} is how you do it:
\lstinputlisting[label={code:projectcode},caption={main.c code used in this project},style=CStyle]{main.c}
\end{document}
